/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.ui.html.bootstrap.unify;

import org.clawiz.core.common.system.module.AbstractModule;

public class UiHtmlBootstrapUnifyModule extends AbstractModule {

    public static final String ASSETS_RESOURCE_PATH               = "/org/clawiz/ui/html/bootstrap/unify/assets";
    public static final String UNIFY_196_ASSETS_RESOURCE_PATH     = ASSETS_RESOURCE_PATH + "/unify/1.9.6";
    public static final String UNIFY_ASSETS_RESOURCE_PATH         = UNIFY_196_ASSETS_RESOURCE_PATH;
    public static final String CLAWIZ_ASSETS_RESOURCE_PATH        = ASSETS_RESOURCE_PATH + "/clawiz";

}
