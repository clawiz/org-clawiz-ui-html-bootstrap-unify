/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.ui.html.bootstrap.unify.generator.application;

import org.clawiz.core.common.CoreException;
import org.clawiz.ui.common.generator.application.GenerateApplicationContext;
import org.clawiz.ui.common.generator.view.form.FormGenerator;
import org.clawiz.ui.common.generator.view.grid.GridGenerator;
import org.clawiz.ui.html.bootstrap.common.generator.application.BootstrapApplicationGenerator;
import org.clawiz.ui.html.bootstrap.unify.UiHtmlBootstrapUnifyModule;
import org.clawiz.ui.html.bootstrap.unify.generator.view.form.UnifyFormGenerator;
import org.clawiz.ui.html.bootstrap.unify.generator.view.grid.UnifyGridGenerator;
import org.clawiz.ui.html.common.generator.application.component.JavaScriptRequireConfigComponent;

public class UnifyApplicationGenerator extends BootstrapApplicationGenerator {


    @Override
    public <T extends GenerateApplicationContext> Class<T> getGenerateApplicationContextClass() {
        return (Class<T>) GenerateUnifyApplicationContext.class;
    }

    @Override
    public <T extends FormGenerator> Class<T> getFormGeneratorClass() {
        return (Class<T>) UnifyFormGenerator.class;
    }

    @Override
    public <T extends GridGenerator> Class<T> getGridGeneratorClass() {
        return (Class<T>) UnifyGridGenerator.class;
    }

    @Override
    protected <T extends JavaScriptRequireConfigComponent> Class<T> getJavaScriptRequireConfigComponentClass() {
        return (Class<T>) UnifyJavaScriptRequireConfigComponent.class;
    }

    @Override
    protected void done() throws CoreException {

        addFilesToContextPrototype(UiHtmlBootstrapUnifyModule.CLAWIZ_ASSETS_RESOURCE_PATH,  "/assets/clawiz/unify");
        addFilesToContextPrototype(UiHtmlBootstrapUnifyModule.UNIFY_ASSETS_RESOURCE_PATH,   "/assets/unify");
        super.done();
    }
}
