/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.ui.html.bootstrap.unify.generator.view.common.component.element.html.input;

import org.clawiz.core.common.CoreException;
import org.clawiz.ui.html.bootstrap.common.generator.view.common.component.element.html.BootstrapFormElement;
import org.clawiz.ui.html.bootstrap.unify.generator.view.form.UnifyFormGenerator;

public class UnifyFormElement extends BootstrapFormElement {


    protected UnifyFormGenerator getInspiniaFormGenerator() {
        return (UnifyFormGenerator) getGenerator();
    }

    @Override
    public void process() throws CoreException {
        super.process();

        setClass("form-horizontal");
        setRole("form");
        setAction(getInspiniaFormGenerator().getServletPrototypeComponent().getServletPath());
        setMethod(METHOD_POST);
    }
}
