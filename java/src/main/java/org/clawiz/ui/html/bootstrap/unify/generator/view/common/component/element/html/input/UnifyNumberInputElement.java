/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.ui.html.bootstrap.unify.generator.view.common.component.element.html.input;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.data.valuetype.ValueTypeNumber;
import org.clawiz.core.xdk.utils.StringUtils;
import org.clawiz.ui.html.bootstrap.common.generator.view.common.component.element.html.input.BootstrapNumberInputElement;
import org.clawiz.ui.html.bootstrap.common.generator.view.common.component.element.html.input.BootstrapTextInputElement;

public class UnifyNumberInputElement extends BootstrapNumberInputElement {

    @Override
    public void process() throws CoreException {
        super.process();

        setClass("form-control");

/*
        String digits = "999999999999999999999";
        String mask   = "999999999999999";

        if ( getInput().getFormField().getValueType() instanceof ValueTypeNumber ) {
            ValueTypeNumber vtn = (ValueTypeNumber) getInput().getFormField().getValueType();
            if ( vtn.getPrecision() > 0 ) {
                mask = StringUtils.substring(digits, 0, vtn.getPrecision());
                if ( vtn.getScale() > 0 ) {
                    mask = StringUtils.substring(mask, 0, mask.length() - vtn.getScale()) + "." + StringUtils.substring(digits, 0, vtn.getScale());
                }
            }
        }

        addAttribute("data-mask", mask);
*/

    }
}
