/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.ui.html.bootstrap.unify.generator.view.form;

import org.clawiz.core.common.CoreException;
import org.clawiz.ui.common.data.layout.input.ComboBoxInput;
import org.clawiz.ui.common.data.layout.input.Input;
import org.clawiz.ui.common.data.layout.input.TextInput;
import org.clawiz.ui.common.data.model.field.choosevalue.ChooseValueContext;
import org.clawiz.ui.html.bootstrap.common.generator.view.form.BootstrapFormGenerator;
import org.clawiz.ui.html.bootstrap.unify.generator.view.form.controller.UnifyFormControllerPrototypeComponent;
import org.clawiz.ui.html.bootstrap.unify.generator.view.form.servlet.component.UnifyFormServletComponent;
import org.clawiz.ui.html.bootstrap.unify.generator.view.form.servlet.component.UnifyFormServletPrototypeComponent;
import org.clawiz.ui.html.common.generator.view.form.controller.JavaScriptFormControllerPrototypeComponent;
import org.clawiz.ui.html.common.generator.view.form.servlet.component.HtmlFormServletComponent;
import org.clawiz.ui.html.common.generator.view.form.servlet.component.HtmlFormServletPrototypeComponent;

public class UnifyFormGenerator extends BootstrapFormGenerator {

    @Override
    protected <T extends HtmlFormServletComponent> Class<T> getServletComponentClass() {
        return (Class<T>) UnifyFormServletComponent.class;
    }

    @Override
    protected <T extends HtmlFormServletPrototypeComponent> Class<T> getServletPrototypeComponentClass() {
        return (Class<T>) UnifyFormServletPrototypeComponent.class;
    }


    @Override
    protected <T extends JavaScriptFormControllerPrototypeComponent> Class<T> getControllerPrototypeComponentClass() {
        return (Class<T>) UnifyFormControllerPrototypeComponent.class;
    }


    public static final String INSPINIA_INPUT_CONTROLLERS_PATH = "inspinia/ui/controller/view/input";


    private String _inputToControllerClassName(Input input) throws CoreException {
        if ( input instanceof TextInput) {
            return "InspiniaText";
        }
        if ( input instanceof ComboBoxInput) {
            ChooseValueContext cvc = input.getFormField().getChooseValueContext();
            if ( cvc.isStaticValues() ) {
                return "InspiniaStaticComboBox";
            } else {
                return "InspiniaDynamicComboBox";
            }
        }

        return null;
    }


    @Override
    public String getInputControllerClassName(Input input) throws CoreException {
        String name = _inputToControllerClassName(input);
        return name != null
                ? (INSPINIA_INPUT_CONTROLLERS_PATH + "/" + name + "InputController")
                : super.getInputControllerClassName(input);
    }
}
