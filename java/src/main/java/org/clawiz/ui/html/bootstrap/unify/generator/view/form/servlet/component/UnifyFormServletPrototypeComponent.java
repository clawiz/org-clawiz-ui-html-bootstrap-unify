/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.ui.html.bootstrap.unify.generator.view.form.servlet.component;

import org.clawiz.core.common.CoreException;
import org.clawiz.ui.common.data.layout.button.DefaultButton;
import org.clawiz.ui.common.data.layout.container.Column;
import org.clawiz.ui.common.data.layout.input.*;
import org.clawiz.ui.common.data.view.form.Form;
import org.clawiz.ui.html.bootstrap.common.generator.view.form.servlet.component.BootstrapFormServletPrototypeComponent;
import org.clawiz.ui.html.bootstrap.unify.generator.view.common.component.element.html.UnifyButtonElement;
import org.clawiz.ui.html.bootstrap.unify.generator.view.form.servlet.component.element.UnifyFormColumnElement;
import org.clawiz.ui.html.bootstrap.unify.generator.view.common.component.element.html.input.*;

public class UnifyFormServletPrototypeComponent extends BootstrapFormServletPrototypeComponent {

    @Override
    protected void prepareLayoutComponentToElementMap() throws CoreException {
        super.prepareLayoutComponentToElementMap();
        setLayoutComponentToElementMap(Form.class, UnifyFormElement.class);

        setLayoutComponentToElementMap(Column.class, UnifyFormColumnElement.class);

        setLayoutComponentToElementMap(TextInput.class,       UnifyTextInputElement.class);
        setLayoutComponentToElementMap(DateInput.class,       UnifyDateInputElement.class);
        setLayoutComponentToElementMap(DateTimeInput.class,   UnifyDateTimeInputElement.class);
        setLayoutComponentToElementMap(NumberInput.class,     UnifyNumberInputElement.class);
        setLayoutComponentToElementMap(ComboBoxInput.class,   UnifyComboBoxInputElement.class);

        setLayoutComponentToElementMap(DefaultButton.class,   UnifyButtonElement.class);
    }
}
