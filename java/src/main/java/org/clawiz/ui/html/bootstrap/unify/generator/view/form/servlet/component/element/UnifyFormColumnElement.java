/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.ui.html.bootstrap.unify.generator.view.form.servlet.component.element;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.abstractgenerator.element.AbstractElement;
import org.clawiz.ui.html.bootstrap.common.generator.view.form.servlet.component.element.BootstrapFormColumnElement;
import org.clawiz.ui.html.common.generator.view.common.component.element.html.input.AbstractHtmlInputElement;

public class UnifyFormColumnElement extends BootstrapFormColumnElement {

    protected int getLabelColumnsCount() {
        return 2;
    }

    protected int getRowColumnsCount() {
        return 12;
    }

    @Override
    public void write() throws CoreException {
        boolean labelExists = false;

        for(AbstractElement element : getElements() ) {
            if ( element instanceof AbstractHtmlInputElement) {
                if ( ((AbstractHtmlInputElement) element).getInput().getLabel() != null ) {
                    labelExists = true;
                    break;
                }
            }

        }

        for(AbstractElement element : getElements() ) {
            contextWriteln("<div class=\"form-group\">");
            if ( labelExists ) {

                String label = null;
                if ( element instanceof AbstractHtmlInputElement) {
                    label = ((AbstractHtmlInputElement) element).getInput().getLabel();
                }

                if ( label != null ) {
                    contextWriteln("<label class=\"col-lg-" + getLabelColumnsCount() +" control-label\">" + encodeJavaQuotes(label != null ? label : "") + "</label>");
                    contextWriteln("<div class=\"col-lg-" + (getRowColumnsCount() - getLabelColumnsCount()) + "\">");
                    element.write();
                    contextWriteln("</div>");
                } else {
                    contextWriteln("<div class=\"col-lg-offset-" + getLabelColumnsCount() + " col-lg-" + (getRowColumnsCount() - getLabelColumnsCount())  +"\">");
                    element.write();
                    contextWriteln("</div>");
                }

            } else {

                element.write();

            }
            contextWriteln("</div>");
        }
    }
}
