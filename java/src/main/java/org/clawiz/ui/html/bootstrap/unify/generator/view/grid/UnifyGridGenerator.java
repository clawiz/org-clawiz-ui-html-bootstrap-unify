/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.ui.html.bootstrap.unify.generator.view.grid;

import org.clawiz.ui.html.bootstrap.common.generator.view.grid.BootstrapGridGenerator;
import org.clawiz.ui.html.bootstrap.unify.generator.view.grid.servlet.component.UnifyGridServletPrototypeComponent;
import org.clawiz.ui.html.common.generator.view.grid.servlet.component.HtmlGridServletPrototypeComponent;

public class UnifyGridGenerator extends BootstrapGridGenerator {

    @Override
    protected <T extends HtmlGridServletPrototypeComponent> Class<T> getServletPrototypeComponentClass() {
        return (Class<T>) UnifyGridServletPrototypeComponent.class;
    }
}
