/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.ui.html.bootstrap.unify.generator.view.grid.servlet.component;

import org.clawiz.core.common.CoreException;
import org.clawiz.ui.html.bootstrap.common.generator.view.grid.servlet.component.BootstrapGridServletPrototypeComponent;
import org.clawiz.ui.html.bootstrap.common.generator.view.grid.servlet.component.element.method.BootstrapGridServletWriteGridHeaderMethodElement;
import org.clawiz.ui.html.bootstrap.common.generator.view.grid.servlet.component.element.method.BootstrapGridServletWriteGridRowMethodElement;
import org.clawiz.ui.html.bootstrap.unify.generator.view.grid.servlet.component.element.method.UnifyGridServletWriteGridHeaderMethodElement;
import org.clawiz.ui.html.bootstrap.unify.generator.view.grid.servlet.component.element.method.UnifyGridServletWriteGridRowMethodElement;

public class UnifyGridServletPrototypeComponent extends BootstrapGridServletPrototypeComponent {

    @Override
    public void prepare() throws CoreException {
        super.prepare();
        addElementClassReplacer(BootstrapGridServletWriteGridHeaderMethodElement.class, UnifyGridServletWriteGridHeaderMethodElement.class);
        addElementClassReplacer(BootstrapGridServletWriteGridRowMethodElement.class, UnifyGridServletWriteGridRowMethodElement.class);
    }
}
