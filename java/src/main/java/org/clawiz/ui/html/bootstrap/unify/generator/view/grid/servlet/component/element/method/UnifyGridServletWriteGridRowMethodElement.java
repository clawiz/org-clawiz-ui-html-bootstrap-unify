/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.ui.html.bootstrap.unify.generator.view.grid.servlet.component.element.method;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.data.valuetype.ValueTypeDate;
import org.clawiz.core.common.metadata.data.valuetype.ValueTypeDateTime;
import org.clawiz.core.common.metadata.data.valuetype.ValueTypeObject;
import org.clawiz.ui.common.data.view.grid.column.ButtonGridColumn;
import org.clawiz.ui.common.data.view.grid.column.GridColumn;
import org.clawiz.ui.html.bootstrap.common.generator.view.grid.servlet.component.element.method.BootstrapGridServletWriteGridRowMethodElement;
import org.clawiz.ui.html.common.generator.view.grid.servlet.component.element.method.HtmlGridServletDoGetMethodElement;

public class UnifyGridServletWriteGridRowMethodElement extends BootstrapGridServletWriteGridRowMethodElement {

    @Override
    public void process() throws CoreException {
        super.process();

        addText("httpContext.writeln(\"      <tr>\");");
        for (GridColumn column : getGrid().getColumns() ) {
            if ( column instanceof ButtonGridColumn ) {
                ButtonGridColumn buttonColumn = (ButtonGridColumn) column;
                String buttonUrl = getHtmlGridJavaClassComponent().getServletPath() + "?" + HtmlGridServletDoGetMethodElement.ACTION_PARAMETER_NAME + "=" + buttonColumn.getName();
                addText("    httpContext.writeln(\"         <td><button type=\\\"button\\\" class=\\\"btn btn-outline btn-primary\\\" href=\\\"#\\\" "
                        + "onclick=\\\"window.open('" + buttonUrl + "&id=\" + row.getId() + \"','_self')\\\""
                        + ">"
                        + buttonColumn.getText()
                        + "</button></td>\");");
            } else {
                if ( column.getValueType() instanceof ValueTypeObject ) {
                    addText("    httpContext.writeln(\"         <td>\" + nvl(row." +  column.getGetToStringMethodName() + "(), \"\") + \"</td>\");");
                } else if ( column.getValueType() instanceof ValueTypeDate) {
                    addText("    httpContext.writeln(\"         <td>\" + dateToString(row." +  column.getGetMethodName() + "()) + \"</td>\");");
                } else if ( column.getValueType() instanceof ValueTypeDateTime) {
                    addText("    httpContext.writeln(\"         <td>\" + dateTimeToString(row." +  column.getGetMethodName() + "()) + \"</td>\");");
                } else {
                    addText("    httpContext.writeln(\"         <td>\" + nvl(row." +  column.getGetMethodName() + "(), \"\") + \"</td>\");");
                }
            }
        }
        addText("httpContext.writeln(\"      </tr>\");");

    }
}
