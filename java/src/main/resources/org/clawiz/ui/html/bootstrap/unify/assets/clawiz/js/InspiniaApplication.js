define(["exports", "clawiz/Application"], function (exports, _Application2) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.InspiniaApplication = undefined;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  var InspiniaApplication = exports.InspiniaApplication = function (_Application) {
    _inherits(InspiniaApplication, _Application);

    function InspiniaApplication() {
      _classCallCheck(this, InspiniaApplication);

      return _possibleConstructorReturn(this, (InspiniaApplication.__proto__ || Object.getPrototypeOf(InspiniaApplication)).apply(this, arguments));
    }

    return InspiniaApplication;
  }(_Application2.Application);
});

//# sourceMappingURL=InspiniaApplication.js.map