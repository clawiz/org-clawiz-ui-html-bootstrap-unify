import {ClawizLoader} from "clawiz/ClawizLoader"

export class InspiniaLoader extends ClawizLoader {

    init() {
        super.init();
        this.logDebug("Inspinia loader initialized");
    }

}

