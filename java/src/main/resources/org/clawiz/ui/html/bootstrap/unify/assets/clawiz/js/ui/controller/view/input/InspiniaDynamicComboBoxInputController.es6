import {StaticComboBoxInputController} from 'clawiz/ui/controller/input/StaticComboBoxInputController'

export class InspiniaDynamicComboBoxInputController extends StaticComboBoxInputController {


    onSelect(value) {

        let input = this.$element.get(0);
        let me    = input.clawizController;

        me.value = value.value;
    }

    doQuery(query, process) {
        let input = this.$element.get(0);
        let me    = input.clawizController;

        let url = me.api.path + '?action=' + me.api.action + '&inputValue=' + query;
        me.logDebug("GET : " + url);
        me.jQuery.get(url, function (result)
        {
            me.logDebug('response : ' + result);
            let data = JSON.parse(result);
            let values = [];
            for(let i=0; i < data.length; i++ ) {
                values.push({
                    name  : data[i].text,
                    value : data[i].value
                });
            }

            return process(values);
        });

    }

    init() {

        let me = this;

        this.jQueryInput.typeahead(
            {
                source: me.doQuery,
                afterSelect : me.onSelect
            }
        );

    }


}