define(['exports', 'clawiz/ui/controller/input/StaticComboBoxInputController'], function (exports, _StaticComboBoxInputController) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.InspiniaDynamicComboBoxInputController = undefined;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var InspiniaDynamicComboBoxInputController = exports.InspiniaDynamicComboBoxInputController = function (_StaticComboBoxInputC) {
        _inherits(InspiniaDynamicComboBoxInputController, _StaticComboBoxInputC);

        function InspiniaDynamicComboBoxInputController() {
            _classCallCheck(this, InspiniaDynamicComboBoxInputController);

            return _possibleConstructorReturn(this, (InspiniaDynamicComboBoxInputController.__proto__ || Object.getPrototypeOf(InspiniaDynamicComboBoxInputController)).apply(this, arguments));
        }

        _createClass(InspiniaDynamicComboBoxInputController, [{
            key: 'onSelect',
            value: function onSelect(value) {

                var input = this.$element.get(0);
                var me = input.clawizController;

                me.value = value.value;
            }
        }, {
            key: 'doQuery',
            value: function doQuery(query, process) {
                var input = this.$element.get(0);
                var me = input.clawizController;

                var url = me.api.path + '?action=' + me.api.action + '&inputValue=' + query;
                me.logDebug("GET : " + url);
                me.jQuery.get(url, function (result) {
                    me.logDebug('response : ' + result);
                    var data = JSON.parse(result);
                    var values = [];
                    for (var i = 0; i < data.length; i++) {
                        values.push({
                            name: data[i].text,
                            value: data[i].value
                        });
                    }

                    return process(values);
                });
            }
        }, {
            key: 'init',
            value: function init() {

                var me = this;

                this.jQueryInput.typeahead({
                    source: me.doQuery,
                    afterSelect: me.onSelect
                });
            }
        }]);

        return InspiniaDynamicComboBoxInputController;
    }(_StaticComboBoxInputController.StaticComboBoxInputController);
});

//# sourceMappingURL=InspiniaDynamicComboBoxInputController.js.map