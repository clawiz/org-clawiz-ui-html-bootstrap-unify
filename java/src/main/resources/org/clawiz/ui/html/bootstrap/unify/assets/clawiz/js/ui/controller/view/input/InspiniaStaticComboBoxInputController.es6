import {StaticComboBoxInputController} from 'clawiz/ui/controller/input/StaticComboBoxInputController'

export class InspiniaStaticComboBoxInputController extends StaticComboBoxInputController {

    onSelect(value) {

        let input = this.$element.get(0);
        let controller = input.clawizController;

        controller.value = value.value;
    }

    init() {

        let me = this;

        let values = [];
        for(let i=0; i < me.values.length; i++ ) {
            values.push({
                name  : me.values[i].text,
                value : me.values[i].value
            });
        }

        this.jQueryInput.typeahead(
            {
                source      : values,
                afterSelect : me.onSelect
            }
        );

    }
}